# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from dal import autocomplete
from django.contrib import messages
from django.contrib.auth.models import AnonymousUser
from django.shortcuts import render, redirect
from django.contrib.auth import login as login_user, logout as logout_user
# Create your views here.
from bootcamp.forms import RegistrationForm, LoginForm, TeamCreationForm
from bootcamp.models import User


def home(request):
    iqube_link = 'iqube.io'
    logged_in = True
    team_assigned = False
    if isinstance(request.user, AnonymousUser):
        logged_in = False
    else:
        if request.user.team_id:
            team_assigned = True
    return render(request, 'home.html', {'iqube_link': iqube_link,
                                         'logged_in': logged_in,
                                         'team_created': team_assigned})


def registration(request):
    form = RegistrationForm()
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            messages.add_message(request, messages.SUCCESS, 'Registered')
            form.save()
            return redirect('home')
    return render(request, 'registration.html', {'form': form})


def login(request):
    if isinstance(request.user, AnonymousUser):
        form = LoginForm()
        if request.method == 'POST':
            form = LoginForm(request.POST)
            if form.is_valid():
                data = form.cleaned_data['username']
                user = User.objects.get(username=form.cleaned_data['username'])
                login_user(request, user)
                return redirect('home')
        return render(request, 'login.html', {'form': form})
    else:
        return redirect('home')


class TeamCreationAutoComplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        query_set = User.objects.all().exclude(team__isnull=False).exclude(username=self.request.user.username)
        if self.q:
            query_set = User.objects.all().filter(username__icontains=self.q).exclude(
                username=self.request.user.username)
        return query_set


def team_creation(request):
    form = TeamCreationForm(request)

    if request.method == 'POST':
        form = TeamCreationForm(request, request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, 'Team Created')
            return redirect('home')
    return render(request, 'team_creation.html', {'form': form})


def logout(request):
    logout_user(request)
    return redirect('home')

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.

class Team(models.Model):
    name = models.CharField(max_length=1000, help_text='Enter the name for your team', unique=True)


class User(AbstractUser):
    class Meta:
        db_table = 'users'

    email = models.EmailField()
    phone_number = models.IntegerField()
    college = models.CharField(max_length=1000)
    department = models.CharField(max_length=1000)
    year = models.IntegerField(
        choices=((1, '1st Year'), (2, '2nd Year'), (3, '3rd Year'), (4, '4th Year'), (5, 'Others')))
    paid_fee = models.BooleanField(default=False)
    team = models.ForeignKey(Team, null=True)

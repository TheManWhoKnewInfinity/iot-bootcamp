from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from datetime import datetime

from dal import autocomplete
from django import forms

from bootcamp.models import User, Team


class RegistrationForm(forms.ModelForm):
    class Meta:
        fields = ['username',
                  'first_name',
                  'last_name',
                  'email',
                  'password',
                  'phone_number',
                  'college',
                  'department',
                  'year',
                  ]
        model = User
        widgets = {
            'password': forms.PasswordInput,
        }

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit("submit", "Register"))

    def is_valid(self):
        if not super(RegistrationForm, self).is_valid():
            return False
        try:
            user = User.objects.get(username=self.cleaned_data['username'])
            self.add_error('username', 'Username Already Exists')
            return False
        except User.DoesNotExist:
            return True

    def save(self, commit=True):
        obj = super(RegistrationForm, self).save(commit=False)
        obj.last_login = datetime.now()
        obj.save()


class LoginForm(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(max_length=20, widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit("submit", "Submit"))

    def is_valid(self):
        if not super(LoginForm, self).is_valid():
            return False
        try:
            user = User.objects.get(username=self.cleaned_data['username'])
            if user.password == self.cleaned_data['password']:
                return True
            else:
                self.add_error(None, 'Username or Password Wrong')
                return False
        except User.DoesNotExist:
            self.add_error(None, 'Username Does not Exist')
            return False


class TeamCreationForm(forms.Form):
    team_name = forms.CharField(max_length=200, help_text='Enter a Name for your team')
    member_1 = forms.ModelChoiceField(required=True,
                                      label='Second Member',
                                      queryset=User.objects.all().exclude(team__isnull=False),
                                      empty_label="Error Fetching Usernames",
                                      widget=autocomplete.ModelSelect2(url='team_creation_autocomplete')
                                      )
    member_2 = forms.ModelChoiceField(required=False,
                                      label='Third Member',
                                      queryset=User.objects.all(),
                                      empty_label="Error Fetching Usernames",
                                      widget=autocomplete.ModelSelect2(url='team_creation_autocomplete')
                                      )
    request = ""

    def __init__(self, request, *args, **kwargs):
        super(TeamCreationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = ''
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit("submit", "Create"))
        self.member_1 = forms.ModelChoiceField(required=True,
                                               label='Second Member',
                                               queryset=User.objects.all().exclude(team__isnull=False).exclude(
                                                   username=request.user.username),
                                               empty_label="Error Fetching Usernames",
                                               widget=autocomplete.ModelSelect2(url='team_creation_autocomplete')
                                               )
        self.member_2 = forms.ModelChoiceField(required=False,
                                               label='Third Member',
                                               queryset=User.objects.all().exclude(team__isnull=False).exclude(
                                                   username=request.user.username),
                                               empty_label="Error Fetching Usernames",
                                               widget=autocomplete.ModelSelect2(url='team_creation_autocomplete')
                                               )
        self.request = request

    def is_valid(self):
        if not super(TeamCreationForm, self).is_valid():
            return False
        try:
            team = Team.objects.get(name=self.cleaned_data['team_name'])
            self.add_error('team_name', 'Team Name already Exists')
            return False
        except Team.DoesNotExist:
            return True

    def save(self):
        team = Team.objects.create(name=self.cleaned_data['team_name'])
        self.request.user.team = team
        self.request.user.save()
        self.cleaned_data['member_1'].team = team
        self.cleaned_data['member_1'].save()
        if self.cleaned_data['member_2'] is not None:
            self.cleaned_data['member_2'].team = team
            self.cleaned_data['member_2'].save()
